import { createServer } from 'http';
import commandLineArgs from 'command-line-args';

const optionDefinition = [{ name: 'port', alias: 'p', type: Number }];
const option = commandLineArgs(optionDefinition);
const port = option.port || 3000;
let count = 0;

const server = createServer((req, res) => {
  if (req.url === '/') {
    count += 1;
    const fullRequest = {
      message: 'Request handled successfully',
      requestCount: count,
    };
    res.write(JSON.stringify(fullRequest));
    res.end();
  }
});

server.listen(port, (error) => {
  if (error) throw error;
  console.log(`The server start on http://localhost:${port}`);
});
